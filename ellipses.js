$(document).ready(function () {

	// Get the URL of the current page
	const currentUrl = window.location.href;
	
	// Check if the URL contains '/article'
	if (currentUrl.includes('/article/')) {

		// Select all elements with class 'payoff-slider'
		const sliders = document.querySelectorAll('.payoff-slider');
		// Remove each element with class 'payoff-slider'
		sliders.forEach((slider) => {
			slider.remove();
		});

		// Select the parent element with class 'payoff-wrapper'
		const parentElement = document.querySelector('.payoff-wrapper');

		// Create a new div element with class 'payoff'
		const newDiv = document.createElement('div');
		newDiv.classList.add('payoff', 'payoff-nav');

		// Set the text content of the new div to your custom text
		const navArticleCurrent = document.querySelector('.article-title');
		newDiv.textContent = navArticleCurrent.textContent;

		// Append the new div to the parent element
		parentElement.appendChild(newDiv);

		// Get all div elements with class 'payoff'
		const divs = document.querySelectorAll('.payoff-nav');

		// Set style for each div element
		divs.forEach((div) => {
			div.style.mixBlendMode = 'normal';
			div.style.marginTop = '10px';
		});

	}


	// Select all the elements with class 'card-img'
	const cardImages = document.querySelectorAll('.card-img, .article-image');
	
	// Generate a random index based on the number of card images
	const randomIndex = Math.floor(Math.random() * cardImages.length);
	
	// Get the URL of the randomly selected card image
	const randomImageUrl = cardImages[randomIndex].src;
	
	// Select the element where you want to set the background image
	const backgroundElement = document.querySelector('.slider-item');
	
	// Set the background image of the element to the randomly selected card image
	backgroundElement.style.backgroundImage = `url(${randomImageUrl})`;

	// Tag Categories Grouping
	function tagCategories() {

		$(".tr-wrap").addClass("hide");

		$(".tr-item").each(function (index) {
			let cmsItem = $(this);
			let itemCategory = cmsItem.find(".tr-category").text();
			$(".tr-contain").each(function (index) {
				let groupCategory = $(this).find(".tr-title").text();
				if (groupCategory === itemCategory) {
					cmsItem.appendTo($(this).find(".tr-list"));
				}
			});
		});

	}

	//tagCategories();

	// Load external html to get around 20 Collection List limitations then fire functions
	$("#ajax-nav-grid").load("/nav-grid #ajax-wrapper", function() {
		tagCategories();
		addActiveClass();
	});

	// Shuffle children elements

	$.fn.shuffleChildren = function () {
		$.each(this.get(), function (index, el) {
			var $el = $(el);
			var $find = $el.children();
			$find.sort(function () {
				return 0.5 - Math.random();
			});
			$el.empty();
			$find.appendTo($el);
		});
	};
	$(".shuffle-children").shuffleChildren();

	// Add .active to nav section

	function addActiveClass() {

		var array_title = [];

		$(".article-title").each(function (i, e) {
			array_title.push($(e).text());
		});
		$(".nav-articles .nav-link").each(function (i, item) {
			item = $(this).text();
			if ($.inArray(item, array_title) !== -1) {
				$(this).addClass("active");
			}
		});

		var array_issue = [];

		$(".article-issue").each(function (i, e) {
			array_issue.push($(e).text());
		});
		$(".nav-issues .nav-link").each(function (i, item) {
			item = $(this).text();
			if ($.inArray(item, array_issue) !== -1) {
				$(this).addClass("active");
			}
		});

		var array_authors = [];

		$(".article-author").each(function (i, e) {
			array_authors.push($(e).text());
		});
		$(".nav-authors .nav-link").each(function (i, item) {
			item = $(this).text();
			if ($.inArray(item, array_authors) !== -1) {
				$(this).addClass("active");
			}
		});

		var array_digital_editors = [];

		$(".article-digital-editor").each(function (i, e) {
			array_digital_editors.push($(e).text());
		});
		$(".nav-digital-editors .nav-link").each(function (i, item) {
			item = $(this).text();
			if ($.inArray(item, array_digital_editors) !== -1) {
				$(this).addClass("active");
			}
		});

		var array_tags = [];

		$(".article-tag").each(function (i, e) {
			array_tags.push($(e).text());
		});
		$(".nav-tag .nav-link").each(function (i, item) {
			item = $(this).text();
			if ($.inArray(item, array_tags) !== -1) {
				$(this).addClass("active");
			}
		});

		// Add index to review text link

		review_count = 1;
		$(".article-review").each(function () {
			$(this).text("Review " + review_count);
			$(this).parent().find("h2").text("Peer Review " + review_count);
			review_count++;
		});

		// Change author URL to article URL

		var use_href;
		$('.replace-with-article-url').each(function () {
			use_href = $(this).find(".nav-link-source").attr("href");
			// console.log(use_href);
			$(this).find(".nav-link").attr("href", use_href);
		});

		// Issue colour styling

		modal_bg_color = $(".content-wrapper").css("background-color");
		modal_text_color = $(".main-wrapper").css("color");

		$(".modal-content").css("background-color", modal_bg_color);
		$(".modal-content").css("color", modal_text_color);
		$(".modal-content::-webkit-scrollbar").css("background-color", modal_bg_color);

	}

}); // Doc Ready end



// Prevent body of page from scrolling when modal is active
$(".modal-trigger").click(function () {
	$("body").css("overflow", "hidden");
});
$(".modal-bg, .modal-close").click(function () {
	$("body").css("overflow", "auto");
});

// Glitch Images

;(function ($, window, document, undefined) {
	"use strict";
	var pluginName = "glitchImage";
	var defaults = {
		imageUrl: null, // uses background unless specified
		frequency: 500 //ms
	};
	//exposed controls
	var controls = {
		play: function (id) {
			Plugin.prototype.play(id);
		},
		pause: function (id) {
			Plugin.prototype.pause(id);
		},
		destroy: function () {
			console.log(Plugin.prototype);
		}
	}
	// The actual plugin constructor
	function Plugin(element, options) {
		this.element = element;
		this.settings = $.extend({}, defaults, options);
		this._defaults = defaults;
		this._name = pluginName;

		this.init();
	}
	// Avoid Plugin.prototype conflicts
	$.extend(Plugin.prototype, {
		init: function () {
			this.$element = $(this.element);
			this.glitchId = parseInt(this.$element.data('glitch-id'));
			if (!this.glitchId) {
				this.glitchId = 0;
			}
			if (!this.settings.width) {
				this.settings.width = this.$element.width();
			}
			if (!this.settings.height) {
				this.settings.height = this.$element.height();
			}
			this.canvas = document.createElement('canvas');
			this.canvas.width = this.settings.width;
			this.canvas.height = this.settings.height;
			//append the canvas to the selected el
			this.$element.append(this.canvas);
			this.context = this.canvas.getContext('2d');

			if (!Array.isArray($.fn[pluginName].paused)) {
				$.fn[pluginName].paused = [];
			}
			if (this.settings.imageUrl === null) {
				this.backgroundImageUrl = this.$element.css('background-image').slice(5, -2);
			} else {
				this.backgroundImageUrl = settings.imageUrl;
			}
			this.img = new Image();
			this.img.src = this.backgroundImageUrl;
			this.img.width = this.settings.width;
			this.img.height = this.settings.height;
			this.draw();
		},
		getRandInt: function (a, b) {
			return ~~(Math.random() * (b - a) + a)
		},
		draw: function () {
			var _this = this;
			this.glitchInterval = setInterval(function () {
				if ($.fn[pluginName].paused.indexOf(_this.glitchId) === -1) {
					setTimeout(function () {
						for (var i = 0; i < _this.getRandInt(1, 13); i++) {
							var x = Math.random() * _this.settings.width,
								y = Math.random() * _this.settings.height,
								spliceWidth = _this.settings.width - x,
								spliceHeight = _this.getRandInt(5, _this.settings.height / 3);
							_this.context.drawImage(_this.img, 0, y, spliceWidth, spliceHeight, x, y, spliceWidth, spliceHeight);
							_this.context.drawImage(_this.img, spliceWidth, y, x, spliceHeight, 0, y, x, spliceHeight);
						}
					}, _this.getRandInt(250, 1000));
				}
			}, _this.settings.frequency);
		},
		clear: function () {
			this.context.clearRect(0, 0, settings.width, settings.height);
		},
		play: function (id) {
			var indexOfPaused = $.fn[pluginName].paused.indexOf(id);
			if (indexOfPaused >= 0) {
				$.fn[pluginName].paused.splice(indexOfPaused, 1);
			}
		},
		pause: function (id) {
			if ($.fn[pluginName].paused.length === 0) {
				$.fn[pluginName].paused = [id];
			} else {
				if ($.fn[pluginName].paused.indexOf(id) === -1) {
					$.fn[pluginName].paused.push(id);
				}
			}
		},
		stop: function () {

		},
		destroy: function () {
		}
	});
	// A really lightweight plugin wrapper around the constructor,
	// preventing against multiple instantiations
	$.fn[pluginName] = function (options) {
		if (typeof arguments[0] === 'string') {
			//execute string comand on mediaPlayer
			//pass the element id as an argument to control functions
			var args = [$(this).data('glitch-id')];
			controls[arguments[0]].apply(this, args);
			return this;
		} else {
			return this.each(function () {
				if (!$.data(this, "plugin_" + pluginName)) {
					$.data(this, "plugin_" +
						pluginName, new Plugin(this, options));
				}
			});
		}
	};

})(jQuery, window, document);
$(document).ready(function () {
	var glitch = $('.glitched').glitchImage();
});



// Animate one character at a time for a string of text

// $( ".payoff" ).each(function( i, index ) {
//   console.log( index + ": " + $( this ).text() );
// });

// // Build array with Payoff
// var textArray = $('.payoff-slider .payoff').map(function(){
// 	return $.trim($(this).text());
// }).get();
// var text = textArray[Math.floor(Math.random() * textArray.length)];
// var elem = $(".payoff");
// var delay = 20;
// $(".payoff").text("");
// var addTextByDelay = function (text, elem, delay) {
// 	if (!elem) {
// 		elem = $("body");
// 	}
// 	if (!delay) {
// 		delay = 300;
// 	}
// 	if (text.length > 0) {
// 		//append first character 
// 		elem.append(text[0]);
// 		setTimeout(
// 			function () {
// 				//Slice text by 1 character and call function again                
// 				addTextByDelay(text.slice(1), elem, delay);
// 			}, delay
// 		);
// 	}
// }
// addTextByDelay(text, elem, delay);



/*
* Webflow Slider Events: Trigger an event on navigation of Webflow's slider components.
* @license MIT
* @author Neal White - http://www.cohesive.cc
*
* https://github.com/cohesivecc/webflow-events-slider
*/
// $(function() {
// 	var Webflow = Webflow || [];
// 	Webflow.push(function () {
// 		var namespace = '.w-slider';
	
// 		function slideChangeEvent(evt) {
// 			var slider;
// 			if ($(evt.target).is(namespace)) {
// 				slider = $(evt.target);
// 			} else {
// 				slider = $(evt.target).closest(namespace)
// 			}
// 			if (slider) {
// 				$(slider).trigger('slider-event', $(slider).data(namespace));
// 			}
// 		}
	
// 		var tap_selector = $.map(['.w-slider-arrow-left', '.w-slider-arrow-right', '.w-slider-dot'], function (s) { return namespace + ' ' + s; }).join(', ');
	
// 		// listeners
// 		$(document).off('tap' + namespace, tap_selector, slideChangeEvent).on('tap' + namespace, tap_selector, slideChangeEvent);
// 		$(document).off('swipe' + namespace, namespace, slideChangeEvent).on('swipe' + namespace, namespace, slideChangeEvent);
	
// 		// initial slide - manually trigger the event
// 		$(namespace + ':visible').each(function (i, s) {
// 			slideChangeEvent({ target: s });
// 		});
	
// 	});
// 	var Webflow = Webflow || [];
// 	Webflow.push(function () {
// 		// listen for all sliders on the page - .w-slider is Webflow's default class for slider components
// 		$(document).on('slider-event', '.w-slider', function(e, data) {
// 			//do your thang
// 			console.log('All Siders triggered')
// 		});
// 		// OR - listen for events from a specific subset of sliders
// 		$(document).on('slider-event', '.payoff-slider', function (e, data) {
// 			// do your thang
// 			addTextByDelay(text, elem, delay);
// 			console.log('Payoff Siders triggered')
// 		});
// 	});
// });