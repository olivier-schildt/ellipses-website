$(document).ready(function () {

	console.log("1");

    // Tag Categories Grouping
    function tagCategories() {
        $(".tr-wrap").addClass("hide");
        $(".tr-item").each(function () {
            let cmsItem = $(this);
            let itemCategory = cmsItem.find(".tr-category").text().trim();
            $(".tr-contain").each(function () {
                let groupCategory = $(this).find(".tr-title").text().trim();
                if (groupCategory === itemCategory) {
                    cmsItem.appendTo($(this).find(".tr-list"));
                }
            });
        });
    }
    tagCategories();

    // Shuffle children elements
    $.fn.shuffleChildren = function () {
        this.each(function () {
            var $el = $(this);
            var $children = $el.children();
            $children.sort(function () {
                return 0.5 - Math.random();
            });
            $el.empty().append($children);
        });
    };
    $(".shuffle-children").shuffleChildren();

    // Add .active to nav section
    function addActiveClassToNav(sectionClass, articleClass) {
        var array_items = $(articleClass).map(function () { return $(this).text().trim(); }).get();
        $(sectionClass + " .nav-link").each(function () {
            var item = $(this).text().trim();
            if (array_items.includes(item)) {
                $(this).addClass("active");
            }
        });
    }

    addActiveClassToNav(".nav-articles", ".article-title");
    addActiveClassToNav(".nav-issues", ".article-issue");
    addActiveClassToNav(".nav-authors", ".article-author");
    addActiveClassToNav(".nav-digital-editors", ".article-digital-editor");
    addActiveClassToNav(".nav-tag", ".article-tag");

    // Add index to review text link
    $(".article-review").each(function (index) {
        $(this).text("Review " + (index + 1));
        $(this).parent().find("h2").text("Peer Review " + (index + 1));
    });

    // Change author URL to article URL
    $('.replace-with-article-url').each(function () {
        var use_href = $(this).find(".nav-link-source").attr("href");
        if (use_href) {
            $(this).find(".nav-link").attr("href", use_href);
        }
    });

    // Issue colour styling
    var modal_bg_color = $(".content-wrapper").css("background-color");
    var modal_text_color = $(".main-wrapper").css("color");
    $(".modal-content").css({ "background-color": modal_bg_color, "color": modal_text_color });

    // Load external HTML then fire functions
    $("#ajax-nav-grid").load("/nav-grid #ajax-wrapper", function (response, status) {
        if (status === "success") {
            addActiveClassToNav(".nav-articles", ".article-title");
            // ... Repeat for other nav sections ...
        }
    });

    // Prevent body scrolling when modal is active
    $(".modal-trigger").click(function () {
        $("body").css("overflow", "hidden");
    });
    $(".modal-bg, .modal-close").click(function () {
        $("body").css("overflow", "auto");
    });

    // Glitch imgToCopy
    function setRandomImage() {
		var imgCards = $('.card-img');
		var navImg = $('.nav-img');
		console.log("imgCards found: ", imgCards.length);
		console.log("navImg found: ", navImg.length);
	
		if (imgCards.length > 0 && navImg.length > 0) {
			var randomIndex = Math.floor(Math.random() * imgCards.length);
			var randomImgSrc = imgCards.eq(randomIndex).attr('src');
			console.log("Random image src: ", randomImgSrc);
			navImg.attr('src', randomImgSrc);
		} else {
			console.log("Required elements not found.");
		}
	}
	setRandomImage();	

    // Glitch Image Plugin
    (function ($, window, document, undefined) {
        "use strict";
        var pluginName = "glitchImage";
        var defaults = {
            frequency: 500 //ms
        };

        function Plugin(element, options) {
            this.element = element;
            this.settings = $.extend({}, defaults, options);
            this._defaults = defaults;
            this._name = pluginName;

            this.init();
        }

        $.extend(Plugin.prototype, {
            init: function () {
                this.$element = $(this.element);
                this.glitchId = parseInt(this.$element.data('glitch-id'), 10) || 0;

                this.canvas = document.createElement('canvas');
                this.context = this.canvas.getContext('2d');

                if (!Array.isArray($.fn[pluginName].paused)) {
                    $.fn[pluginName].paused = [];
                }

                this.backgroundImageUrl = this.$element.attr('src');
                this.img = new Image();
                this.img.src = this.backgroundImageUrl;
                this.img.onload = () => {
                    this.canvas.width = this.img.naturalWidth;
                    this.canvas.height = this.img.naturalHeight;
                    this.settings.width = this.img.naturalWidth;
                    this.settings.height = this.img.naturalHeight;
                    this.$element.after(this.canvas);
                    this.$element.hide();
                    this.draw();
                };
            },
            getRandInt: function (a, b) {
                return Math.random() * (b - a) + a;
            },
            draw: function () {
                var _this = this;
                this.glitchInterval = setInterval(function () {
                    if ($.fn[pluginName].paused.indexOf(_this.glitchId) === -1) {
                        setTimeout(function () {
                            for (var i = 0; i < _this.getRandInt(1, 13); i++) {
                                var x = Math.random() * _this.settings.width,
                                    y = Math.random() * _this.settings.height,
                                    spliceWidth = _this.settings.width - x,
                                    spliceHeight = _this.getRandInt(5, _this.settings.height / 3);
                                _this.context.drawImage(_this.img, 0, y, spliceWidth, spliceHeight, x, y, spliceWidth, spliceHeight);
                                _this.context.drawImage(_this.img, spliceWidth, y, x, spliceHeight, 0, y, x, spliceHeight);
                            }
                        }, _this.getRandInt(250, 1000));
                    }
                }, _this.settings.frequency);
            },
            clear: function () {
                this.context.clearRect(0, 0, this.settings.width, this.settings.height);
            },
            play: function (id) {
                var indexOfPaused = $.fn[pluginName].paused.indexOf(id);
                if (indexOfPaused >= 0) {
                    $.fn[pluginName].paused.splice(indexOfPaused, 1);
                }
            },
            pause: function (id) {
                var indexOfPaused = $.fn[pluginName].paused.indexOf(id);
                if (indexOfPaused === -1) {
                    $.fn[pluginName].paused.push(id);
                }
            },
            destroy: function () {
                clearInterval(this.glitchInterval);
                this.$element.show();
                $(this.canvas).remove();
                $.removeData(this.element, "plugin_" + pluginName);
            }
        });

        $.fn[pluginName] = function (options) {
            if (typeof arguments[0] === 'string') {
                var args = [$(this).data('glitch-id')];
                Plugin.prototype[arguments[0]].apply(this, args);
                return this;
            } else {
                return this.each(function () {
                    if (!$.data(this, "plugin_" + pluginName)) {
                        $.data(this, "plugin_" + pluginName, new Plugin(this, options));
                    }
                });
            }
        };

    })(jQuery, window, document);

    $('.glitched').glitchImage();
});